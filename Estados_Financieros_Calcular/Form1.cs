﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estados_Financieros_Calcular
{
    public partial class Form1 : Form
    {
        Stopwatch sw = new Stopwatch();
        private string connetionString = "Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda";
        private readonly BackgroundWorker _bw = new BackgroundWorker();
        public static class Compania
        {
            public static string CodCompania;
        }
        public Form1(string com)
        {
             Compania.CodCompania = com;
            InitializeComponent();

            
        }

        private void buttonGenerar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Este proceso podría durar de 2-3 min dependiendo de la cantidad de la información, ¿desea generarlo?.", "Warning",
MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                progressBar1.Style = ProgressBarStyle.Marquee;

                progressBar1.MarqueeAnimationSpeed = 100;
                sw.Start();
                proceso_segundo_plano = null;
                proceso_segundo_plano = new BackgroundWorker();
                proceso_segundo_plano.WorkerReportsProgress = true;
                proceso_segundo_plano.WorkerSupportsCancellation = true;
                proceso_segundo_plano.DoWork += new DoWorkEventHandler(corriendo_proceso);
                proceso_segundo_plano.ProgressChanged += new ProgressChangedEventHandler(progreso_proceso);
                proceso_segundo_plano.RunWorkerCompleted += new RunWorkerCompletedEventHandler(proceso_completo);

                //Invocar el proceso.
                proceso_segundo_plano.RunWorkerAsync();
            }
            else if (result == DialogResult.No)
            {
                this.Close();
            }
            else if (result == DialogResult.Cancel)
            {
                this.Close();
            }
            //CargarDatos();
            // sw.Stop();
            //MessageBox.Show(sw.Elapsed.ToString());
            //this.Close();

        }
       public void CargarDatos()
        {
            string pais = "";
            switch (Compania.CodCompania.ToString())
            {
                case "EMASAL":
                    pais = "EL SALVADOR";
                    break;
                case "EMASALCR":
                    pais = "COSTA RICA";
                    break;
                case "EMASALGT":
                    pais = "GUATEMALA";
                    break;
                case "EMASALHN":
                    pais = "HONDURAS";
                    break;
                case "EMASALNI":
                    pais = "NICARAGUA";
                    break;
                case "EMASALPTY":
                    pais = "PANAMA";
                    break;
                case "EMASALPA":
                    pais = "EMASALPA";
                    break;
                case "QUESTOR":
                    pais = "QUESTOR";
                    break;
                case "PTE":
                    pais = "PTE";
                    break;

            }
           
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            cnn = new SqlConnection(connetionString);
            cnn.Open();

            if (RdTotal.Checked == true)
            {



                string sql = "DELETE DBO.EF_Automaticos WHERE PAIS='" + pais + "' AND MES='" + dateTimeFechaFin.Value.Month + "' AND ANHO='" + dateTimeFechaFin.Value.Year + "'";
                SqlCommand cmd = new SqlCommand(sql, cnn);
                cmd.CommandTimeout = 1200;
                cmd.ExecuteNonQuery();
                Cursor.Current = Cursors.WaitCursor;

                string sql2 = "EXEC DBO.Llenar_EF_Automaticos  @pais='" + pais + "',@fechaSaldoInicio='" + dateTimeFechaInicio.Value.ToString("yyyy/MM/dd") + "',@fechaSaldoFin='" + dateTimeFechaFin.Value.ToString("yyyy/MM/dd") + "',@fechaAcumuladoInicio = '" + dateTimeFechaInAcu.Value.ToString("yyyy/MM/dd") + "',@fechaAcumuladoFin = '" + dateTimeFechaFinAcu.Value.ToString("yyyy/MM/dd") + "'";
                SqlCommand cmd2 = new SqlCommand(sql2, cnn);
                cmd2.CommandTimeout = 1200;

                cmd2.ExecuteNonQuery();
                string sql3 = "EXEC DBO.GetMovCuentasCG  @pais='" + pais + "',@desde='" + dateTimeFechaInicio.Value.ToString("yyyy/MM/dd") + "',@hasta='" + dateTimeFechaFin.Value.ToString("yyyy/MM/dd") + "'";
                SqlCommand cmd3 = new SqlCommand(sql3, cnn);
                cmd3.CommandTimeout = 1200;

                cmd3.ExecuteNonQuery();

                string sql4 = "EXEC DBO.GetMovCuentasCG  @pais='" + pais + "',@desde='" + dateTimeFechaInAcu.Value.ToString("yyyy/MM/dd") + "',@hasta='" + dateTimeFechaFinAcu.Value.ToString("yyyy/MM/dd") + "'";
                SqlCommand cmd4 = new SqlCommand(sql4, cnn);
                cmd4.CommandTimeout = 1200;

                cmd4.ExecuteNonQuery();
            }
            else
            {
                if(RdCmov.Checked==true)
                {

                    string sql0 = "DELETE dbo.cc_Act_EF WHERE PAIS='" + pais + "'";
                    SqlCommand cmd0 = new SqlCommand(sql0, cnn);
                    cmd0.CommandTimeout = 1200;
                    cmd0.ExecuteNonQuery();

                    string sql01 = "  EXECUTE  dbo.llenar_cc_act @pais='" + pais + "'";
                    SqlCommand cmd01 = new SqlCommand(sql01, cnn);
                    cmd01.CommandTimeout = 1200;
                    cmd01.ExecuteNonQuery();

                    string sql = "DELETE DBO.EF_Automaticos WHERE PAIS='" + pais + "' AND MES='" + dateTimeFechaFin.Value.Month + "' AND ANHO='" + dateTimeFechaFin.Value.Year + "' AND CUENTA_CONTABLE IN (SELECT CUENTA_CONTABLE FROM dbo.cc_Act_EF WHERE PAIS='"+pais+"')";
                    SqlCommand cmd = new SqlCommand(sql, cnn);
                    cmd.CommandTimeout = 1200;
                    cmd.ExecuteNonQuery();
                    Cursor.Current = Cursors.WaitCursor;

                    string sql2 = "EXEC DBO.Llenar_EF_Automaticos_act  @pais='" + pais + "',@fechaSaldoInicio='" + dateTimeFechaInicio.Value.ToString("yyyy/MM/dd") + "',@fechaSaldoFin='" + dateTimeFechaFin.Value.ToString("yyyy/MM/dd") + "',@fechaAcumuladoInicio = '" + dateTimeFechaInAcu.Value.ToString("yyyy/MM/dd") + "',@fechaAcumuladoFin = '" + dateTimeFechaFinAcu.Value.ToString("yyyy/MM/dd") + "'";
                    SqlCommand cmd2 = new SqlCommand(sql2, cnn);
                    cmd2.CommandTimeout = 1200;

                    cmd2.ExecuteNonQuery();
                    string sql3 = "EXEC DBO.GetMovCuentasCG_Act  @pais='" + pais + "',@desde='" + dateTimeFechaInicio.Value.ToString("yyyy/MM/dd") + "',@hasta='" + dateTimeFechaFin.Value.ToString("yyyy/MM/dd") + "'";
                    SqlCommand cmd3 = new SqlCommand(sql3, cnn);
                    cmd3.CommandTimeout = 1200;

                    cmd3.ExecuteNonQuery();

                    string sql4 = "EXEC DBO.GetMovCuentasCG_Act  @pais='" + pais + "',@desde='" + dateTimeFechaInAcu.Value.ToString("yyyy/MM/dd") + "',@hasta='" + dateTimeFechaFinAcu.Value.ToString("yyyy/MM/dd") + "'";
                    SqlCommand cmd4 = new SqlCommand(sql4, cnn);
                    cmd4.CommandTimeout = 1200;

                    cmd4.ExecuteNonQuery();
                }
            }


            
            Cursor.Current = Cursors.Default;
        }

        private void dateTimeFechaInAcu_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string fechaIni = "01/" + dt.Month.ToString() +"/"+ dt.Year.ToString();
            string fechaFin = System.DateTime.DaysInMonth( dt.Year,  dt.Month).ToString()+"/" + dt.Month.ToString() + "/" + dt.Year.ToString();
            string FechaInAcu =  "01/01/" + dt.Year.ToString();
            string FechaFinAcu = "01/01/" + dt.Year.ToString();
            DateTime f = Convert.ToDateTime(fechaFin);
            dateTimeFechaInicio.Value = Convert.ToDateTime(fechaIni);
            dateTimeFechaFin.Value = Convert.ToDateTime(fechaFin);
            dateTimeFechaInAcu.Value = Convert.ToDateTime(FechaInAcu);
            dateTimeFechaFinAcu.Value = f;

        }

        private void dateTimeFechaFin_ValueChanged(object sender, EventArgs e)
        {
            DateTime f = dateTimeFechaFin.Value;
            dateTimeFechaFinAcu.Value = f;
        }

        private void tmtrContador_Tick(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        

        private void corriendo_proceso(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Cuando se preciona en detener vuelve esta valor verdadero.
               
                {
                    Thread.Sleep(0010); //Tiempo de espera. 
                   
                    CargarDatos();
                }
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show("Ha ocurrido un error en el proceso. \n\nDetalles del error: \n\n" + excepcion.Message,
                                            "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return;
        }
        private void proceso_completo(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //Mensaje de Error.
            }
              
                else
                {   //CargarDatos();
                    //sw.Stop();
                    //
                progressBar1.MarqueeAnimationSpeed = 0;
                //MessageBox.Show("termino");
                MessageBox.Show(sw.Elapsed.ToString());
                this.Close();
                }
            
            return;
        }
        private void progreso_proceso(object sender, ProgressChangedEventArgs e)
        {
            // Modificar objetos que son creados en otro hilo.
            // y para disparar un evento en este metodo para notificar a otros metodos o clases
            //....
            return;
        }
    }
}
