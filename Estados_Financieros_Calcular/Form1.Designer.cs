﻿namespace Estados_Financieros_Calcular
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dateTimeFechaInAcu = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFechaFinAcu = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFechaFin = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonGenerar = new System.Windows.Forms.Button();
            this.grpPeriodo = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.proceso_segundo_plano = new System.ComponentModel.BackgroundWorker();
            this.RdTotal = new System.Windows.Forms.RadioButton();
            this.RdCmov = new System.Windows.Forms.RadioButton();
            this.grpPeriodo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimeFechaInAcu
            // 
            this.dateTimeFechaInAcu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeFechaInAcu.Location = new System.Drawing.Point(21, 71);
            this.dateTimeFechaInAcu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimeFechaInAcu.Name = "dateTimeFechaInAcu";
            this.dateTimeFechaInAcu.Size = new System.Drawing.Size(121, 26);
            this.dateTimeFechaInAcu.TabIndex = 0;
            this.dateTimeFechaInAcu.Value = new System.DateTime(2019, 3, 26, 0, 0, 0, 0);
            this.dateTimeFechaInAcu.ValueChanged += new System.EventHandler(this.dateTimeFechaInAcu_ValueChanged);
            // 
            // dateTimeFechaFinAcu
            // 
            this.dateTimeFechaFinAcu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeFechaFinAcu.Location = new System.Drawing.Point(21, 157);
            this.dateTimeFechaFinAcu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimeFechaFinAcu.Name = "dateTimeFechaFinAcu";
            this.dateTimeFechaFinAcu.Size = new System.Drawing.Size(121, 26);
            this.dateTimeFechaFinAcu.TabIndex = 1;
            this.dateTimeFechaFinAcu.Value = new System.DateTime(2019, 3, 26, 0, 0, 0, 0);
            // 
            // dateTimeFechaInicio
            // 
            this.dateTimeFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeFechaInicio.Location = new System.Drawing.Point(8, 71);
            this.dateTimeFechaInicio.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimeFechaInicio.Name = "dateTimeFechaInicio";
            this.dateTimeFechaInicio.Size = new System.Drawing.Size(121, 26);
            this.dateTimeFechaInicio.TabIndex = 2;
            this.dateTimeFechaInicio.Value = new System.DateTime(2019, 3, 26, 0, 0, 0, 0);
            // 
            // dateTimeFechaFin
            // 
            this.dateTimeFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeFechaFin.Location = new System.Drawing.Point(8, 157);
            this.dateTimeFechaFin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimeFechaFin.Name = "dateTimeFechaFin";
            this.dateTimeFechaFin.Size = new System.Drawing.Size(121, 26);
            this.dateTimeFechaFin.TabIndex = 3;
            this.dateTimeFechaFin.Value = new System.DateTime(2019, 3, 26, 0, 0, 0, 0);
            this.dateTimeFechaFin.ValueChanged += new System.EventHandler(this.dateTimeFechaFin_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Desde";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 131);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hasta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Desde";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 131);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Hasta";
            // 
            // buttonGenerar
            // 
            this.buttonGenerar.Image = ((System.Drawing.Image)(resources.GetObject("buttonGenerar.Image")));
            this.buttonGenerar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerar.Location = new System.Drawing.Point(111, 279);
            this.buttonGenerar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonGenerar.Name = "buttonGenerar";
            this.buttonGenerar.Size = new System.Drawing.Size(162, 35);
            this.buttonGenerar.TabIndex = 8;
            this.buttonGenerar.Text = "Generar";
            this.buttonGenerar.UseVisualStyleBackColor = true;
            this.buttonGenerar.Click += new System.EventHandler(this.buttonGenerar_Click);
            // 
            // grpPeriodo
            // 
            this.grpPeriodo.Controls.Add(this.dateTimeFechaFin);
            this.grpPeriodo.Controls.Add(this.label4);
            this.grpPeriodo.Controls.Add(this.dateTimeFechaInicio);
            this.grpPeriodo.Controls.Add(this.label3);
            this.grpPeriodo.Location = new System.Drawing.Point(14, 15);
            this.grpPeriodo.Name = "grpPeriodo";
            this.grpPeriodo.Size = new System.Drawing.Size(172, 218);
            this.grpPeriodo.TabIndex = 9;
            this.grpPeriodo.TabStop = false;
            this.grpPeriodo.Text = "Periodo Actual";
            this.grpPeriodo.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimeFechaFinAcu);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimeFechaInAcu);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(214, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(172, 218);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Periodo Acumulado";
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Location = new System.Drawing.Point(411, 288);
            this.domainUpDown1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(180, 26);
            this.domainUpDown1.TabIndex = 11;
            this.domainUpDown1.Text = "domainUpDown1";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(-10, 309);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(430, 35);
            this.progressBar1.TabIndex = 12;
            // 
            // proceso_segundo_plano
            // 
            this.proceso_segundo_plano.DoWork += new System.ComponentModel.DoWorkEventHandler(this.corriendo_proceso);
            this.proceso_segundo_plano.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.progreso_proceso);
            this.proceso_segundo_plano.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.proceso_completo);
            // 
            // RdTotal
            // 
            this.RdTotal.AutoSize = true;
            this.RdTotal.Location = new System.Drawing.Point(22, 239);
            this.RdTotal.Name = "RdTotal";
            this.RdTotal.Size = new System.Drawing.Size(69, 24);
            this.RdTotal.TabIndex = 13;
            this.RdTotal.TabStop = true;
            this.RdTotal.Text = "Total";
            this.RdTotal.UseVisualStyleBackColor = true;
            // 
            // RdCmov
            // 
            this.RdCmov.AutoSize = true;
            this.RdCmov.Location = new System.Drawing.Point(214, 239);
            this.RdCmov.Name = "RdCmov";
            this.RdCmov.Size = new System.Drawing.Size(157, 24);
            this.RdCmov.TabIndex = 14;
            this.RdCmov.TabStop = true;
            this.RdCmov.Text = "Cuentas con mov";
            this.RdCmov.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 337);
            this.Controls.Add(this.RdCmov);
            this.Controls.Add(this.RdTotal);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.domainUpDown1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpPeriodo);
            this.Controls.Add(this.buttonGenerar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Grupo Emasal  - Calculo EEFF";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpPeriodo.ResumeLayout(false);
            this.grpPeriodo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimeFechaInAcu;
        private System.Windows.Forms.DateTimePicker dateTimeFechaFinAcu;
        private System.Windows.Forms.DateTimePicker dateTimeFechaInicio;
        private System.Windows.Forms.DateTimePicker dateTimeFechaFin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonGenerar;
        private System.Windows.Forms.GroupBox grpPeriodo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker proceso_segundo_plano;
        private System.Windows.Forms.RadioButton RdTotal;
        private System.Windows.Forms.RadioButton RdCmov;
    }
}

